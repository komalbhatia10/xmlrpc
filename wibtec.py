import xmlrpclib
url = 'http://localhost:8069'
db = 'wibtec_blackbox_v-11e'
username = 'admin'
password = 'admin'
common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
print common.version()
uid = common.authenticate(db, username, password, {})
models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))
print models.execute_kw(db, uid, password,
                        'res.partner', 'check_access_rights',
                        ['read'], {'raise_exception': False})
print models.execute_kw(db, uid, password, 'delivery.carrier',
                        'search', [[['delivery_type', '=', 'fixed']]])
'''@api.model
    def get_carrier_packages(self, delivery_type=None):
        print("\n\n get_carrier_packages>>>>>>>>>>>>", delivery_type)
        domain = []
        if delivery_type:
            if delivery_type['delivery_type']:
                domain.append(
                    ('delivery_type', '=', delivery_type['delivery_type']))

        delivery_ids = self.search(domain)
        res = []
        for ids in delivery_ids:
            res.append((ids.name, ids.delivery_type, ids.fixed_price))
        return res
'''
print "------------------------------------------"
print models.execute_kw(db, uid, password, 'delivery.carrier',
                        'get_carrier_packages', [])
print "-----------------------------------------"
print models.execute_kw(db, uid, password, 'delivery.carrier',
                        'get_carrier_packages', [{'delivery_type': 'base_on_rule'}])
