import xmlrpclib
info = xmlrpclib.ServerProxy('https://demo.odoo.com/start')
print info.version()
# url = 'http://localhost:8069'
# db = 'hr_custom_v-10c'
# username = 'admin'
# password = 'admin'

url = 'https://demo2.odoo.com'
db = 'demo_110_1520321201'
username = 'admin'
password = 'admin'
common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})
models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))
print models.execute_kw(db, uid, password,
                        'res.partner', 'check_access_rights',
                        ['read'], {'raise_exception': False})
print models.execute_kw(db, uid, password,
                        'res.partner', 'search',
                        [[['is_company', '=', True], ['customer', '=', True]]])
print models.execute_kw(db, uid, password,
                        'res.partner', 'search',
                        [[['is_company', '=', True], ['customer', '=', True]]],
                        {'offset': 10, 'limit': 5})
print "----------------------"
print models.execute_kw(db, uid, password,
                        'res.partner', 'search_count',
                        [[['is_company', '=', True], ['customer', '=', True]]])
ids = models.execute_kw(db, uid, password,
                        'res.partner', 'search',
                        [[['is_company', '=', True], ['customer', '=', True]]],
                        {'limit': 1})
[record] = models.execute_kw(db, uid, password,
                             'res.partner', 'read', [ids])
# count the number of fields fetched by default
print len(record)
